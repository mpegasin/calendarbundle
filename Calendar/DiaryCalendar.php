<?php
/**
 * This file is part of GarantCalendar project.
 * Created by: Mikhail Pegasin
 * Date: 30.05.16
 * Time: 11:10
 */

namespace Garant\CalendarBundle\Calendar;

use Garant\CalendarBundle\Model\Calendar;
use Garant\CalendarBundle\Model\UserDependentInterface;
use Garant\CalendarBundle\Traits\UserDependentTrait;
use Symfony\Component\Translation\TranslatorInterface;

class DiaryCalendar extends Calendar implements UserDependentInterface, \JsonSerializable
{
    const CALENDAR_CODE = 'diary';

    use UserDependentTrait;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function jsonSerialize()
    {
        return [
            'code' => $this->getCode(),
            'name' => $this->getName()
        ];
    }

    public function getCode()
    {
        return static::CALENDAR_CODE;
    }

    public function getName()
    {
        if ($this->translator) {
            return $this->translator->trans(static::CALENDAR_CODE);
        } else {
            return static::CALENDAR_CODE;
        }

    }

    public function getUserIds()
    {
        if (method_exists($this->getUser(), 'getId')) {
            return [$this->getUser()->getId()];
        } else {
            throw new \Exception('User is not have getId method');
        }

    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

}