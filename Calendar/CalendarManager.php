<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 16:08
 */

namespace Garant\CalendarBundle\Calendar;


use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\ClosureExpressionVisitor;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Garant\CalendarBundle\Model\AccessCheckableInterface;
use Garant\CalendarBundle\Model\CalendarInterface;
use Garant\CalendarBundle\Model\EventChangableInterface;
use Garant\CalendarBundle\Model\UserDependentInterface;
use Garant\CalendarBundle\Entity\CommonCalendarEvent as Event;

/**
 * Class CalendarManager
 * @package Garant\FullCalendarBundle
 */
class CalendarManager
{
    private $calendars;
    public $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function addCalendar(CalendarInterface $calendar)
    {
        $this->calendars[$calendar->getCode()] = $calendar;
    }

    public function getCalendars()
    {
        return $this->calendars;
    }

    /**
     * @param $calendarCode
     * @return \Garant\CalendarBundle\Model\Calendar
     * @throws \Exception if calendar code is not exist
     * @throws \Exception if calendar is not instance of CalendarInterface
     */
    public function getCalendarByCode($calendarCode)
    {
        if (array_key_exists($calendarCode, $this->calendars)) {
            $calendar = $this->calendars[$calendarCode];
            if ($calendar instanceof CalendarInterface) {
                return $calendar;
            } else {
                throw new \InvalidArgumentException('Calendar must be instance of CalendarInterface. ' . get_class($calendar) . ' given instead.');
            }
        } else {
            throw new \OutOfRangeException('Code ' . $calendarCode . ' is not exist');
        }
    }

    public function getAvailableCalendars($userId)
    {
        $availableCalendars = array();
        foreach ($this->getCalendars() as $code => $calendar) {
            if ($this->checkAccessByCode($code, $userId))
                $availableCalendars[$code] = $calendar;
        }
        return $availableCalendars;
    }

    /**
     * @param $calendarCode
     * @param $userId
     * @return bool false - if access denied to calendar with $calendarCode for user with $userId
     */
    public function checkAccessByCode($calendarCode, $userId)
    {
        $result = false;
        $calendar = $this->getCalendarByCode($calendarCode);
        if ($calendar instanceof UserDependentInterface) {
            $calendar->setUser($userId);
        }
        if ($calendar instanceof AccessCheckableInterface) {
            if ($calendar->checkAccess($userId))
                $result = true;
        } else {
            if (in_array($userId, $calendar->getUserIds()))
                $result = true;
        }
        return $result;
    }

    public function getUserIdsByCode($calendarCode, $userId)
    {
        $calendar = $this->getCalendarByCode($calendarCode);

        if ($calendar instanceof UserDependentInterface) {
            $calendar->setUser($userId);
        }
        return $calendar->getUserIds();
    }

    /**
     * @param $calendarCode
     * @return \Garant\CalendarBundle\Model\CalendarEvent
     * @throws \Exception
     */
    public function createEventByCalendarCode($calendarCode)
    {
        $calendar = $this->getCalendars()[$calendarCode];
        if ($calendar instanceof EventChangableInterface) {
            return $calendar->createCalendarEvent();
        } else {
            throw new \Exception('Calendar must implement EventChangableInterface. Implements ' . implode(', ', get_declared_interfaces($calendar)) . ' only.');
        }
    }

    public function getRepositoryByCalendarCode($calendarCode)
    {
        $reflection = new \ReflectionClass($this->getCalendarByCode($calendarCode)->getCalendarEventClass());
        return $this->em->getRepository('GarantCalendarBundle:' . $reflection->getShortName());
    }

    public function getEventById($calendarCode, $id)
    {
        $event = $this->getRepositoryByCalendarCode($calendarCode)->find($id);
        return $event;
    }

    /**
     * @param $calendarCode
     * @param $userId
     * @param Criteria $criteria
     * @return Event[] $events
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getEventsByCalendarCode($calendarCode, $userId, Criteria $criteria)
    {
            $calendar = $this->getCalendarByCode($calendarCode);
            if ($calendar instanceof UserDependentInterface) {
                $calendar->setUser($userId);
            }
            $userIds = $calendar->getUserIds();
            $qb = $this->getRepositoryByCalendarCode($calendarCode)->createQueryBuilder('e')
                ->where('e.calendarCode = :calendarCode')
                ->andWhere('e.creator IN(:creators)')
                ->andWhere('e.deletedAt IS NULL')
                ->setParameters(array('calendarCode'=>$calendarCode, 'creators'=>$userIds));
            if ($criteria) {
                $qb
                    ->addCriteria($criteria);
            }


            $events = $qb
                ->getQuery()
                ->getResult();

            return $events;
    }
}