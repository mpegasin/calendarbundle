<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 30.05.16
 * Time: 14:42
 */

namespace Garant\CalendarBundle\Model;


abstract class Calendar implements CalendarInterface, EventChangableInterface
{

    protected $calendarEventClass;

    public function __construct($calendarEventClass)
    {
        $this->calendarEventClass = $calendarEventClass;
    }

    /**
     * @return CalendarEventInterface
     * @throws \Exception
     */
    public function createCalendarEvent()
    {
        $calendarEvent = new $this->calendarEventClass;
        if ($calendarEvent instanceof CalendarEventInterface) {
            $calendarEvent->setCalendarCode($this->getCode());
            return $calendarEvent;
        } else {
            throw new \Exception('CalendarEvent must be instance of CalendarEventInterface. ' . get_class($calendarEvent) . 'is given.');
        }
    }

    public function getCalendarEventClass()
    {
        return $this->calendarEventClass;
    }

    public function __toString()
    {
        return $this->getName();
    }

    abstract public function getCode();
    abstract public function getName();
    abstract public function getUserIds();
}