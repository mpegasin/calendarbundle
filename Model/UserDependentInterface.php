<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 16:06
 */

namespace Garant\CalendarBundle\Model;


interface UserDependentInterface {
    public function setUser($userId);
    public function getUser();
}