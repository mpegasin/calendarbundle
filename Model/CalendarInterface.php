<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 15:48
 */

namespace Garant\CalendarBundle\Model;


interface CalendarInterface {
    /**
     * @return mixed Calendar code
     */
    public function getCode();

    /**
     * @return mixed Calendar humanized name
     */
    public function getName();

    /**
     * This method must realize algorithm, which create a set (category) of users, who can create events in current calendar.
     * @return int[] The array structure of user id's, who can create events in current calendar. Example: [1,4,7]
     */
    public function getUserIds();
}