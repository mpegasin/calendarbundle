<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 15:58
 */

namespace Garant\CalendarBundle\Model;


interface AccessCheckableInterface
{
    public function checkAccess($userId);
}