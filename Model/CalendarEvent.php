<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 17:05
 */

namespace Garant\CalendarBundle\Model;

class CalendarEvent implements CalendarEventInterface
{

    const HIGH_PRIORITY_KEY = 0;
    const MIDDLE_PRIORITY_KEY = 1;
    const LOW_PRIORITY_KEY = 2;

    const HIGH_PRIORITY = 'high';
    const MIDDLE_PRIORITY = 'middle';
    const LOW_PRIORITY = 'low';
    /**
     * If event continues all day (for example Birthday)
     * @var boolean
     */
    protected $allDay;
    /**
     * The date and the time of event starting
     * @var \DateTime
     */
    protected $dateTimeStart;
    /**
     * The date and the time of event stopping
     * @var \DateTime
     */
    protected $dateTimeStop;

    /**
     * The title of the event
     * @var string
     */
    protected $name;

    /**
     * The comment to event
     * @var string
     */
    protected $comment;
    /**
     * Id of eventType which can content some fields (for example default background color, border color, font color)
     * @var int
     */
    protected $eventType;

    /**
     * Id of event creator
     * @var int
     */
    protected $creator;

    /**
     * Calendar code the event is created for
     * @var string
     */
    protected $calendarCode;
    /**
     * Is the event visible
     * @var boolean
     */
    protected $visible;

    /**
     * @var int
     */
    protected $priority;

    /**
     * Code of custom background color
     * @var string
     */
    protected $backgroundColor;

    /**
     * Code of custom border color
     * @var string
     */
    protected $borderColor;

    /**
     * Code of custom font color
     * @var string
     */
    protected $fontColor;

    public function setCalendarCode($calendarCode)
    {
        $this->calendarCode = $calendarCode;

        return $this;
    }
}
