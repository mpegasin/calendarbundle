<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 17:04
 */

namespace Garant\CalendarBundle\Model;


interface CalendarEventInterface {
    public function setCalendarCode($calendarCode);
}