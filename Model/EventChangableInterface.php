<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 01.06.16
 * Time: 9:05
 */

namespace Garant\CalendarBundle\Model;


interface EventChangableInterface {
    public function createCalendarEvent();
}