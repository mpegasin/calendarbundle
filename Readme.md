#GarantCalendarBundle
Calendar bundle contents default diary and provides custom calendars adding
##Installation
### With composer

This bundle can be installed using [composer](https://getcomposer.org/):

    composer require garant/calendar-bundle "dev-develop"

### Register the bundle

    <?php

    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(

            // ...
            new Garant\CalendarBundle\GarantCalendarBundle(),
        );

    	// ...
    }
### User

To define creator entity class:

    // app/config/config.php
    
    garant_calendar:
        creator_entity_class: Application\Garant\ECMStaffBundle\Entity\Employee

UserDependentTrait works with instances of \FOS\UserBundle\Model\User and \FOS\UserBundle\Model\UserManager.

### Dependencies


##Usage

###Calendars

GarantCalendarBundle provides the default Diary calendar. The diary has code "diary" and event class
 "Garant\CalendarBundle\Entity\CommonCalendarEvent". The name of the diary calendar depends from locale
  ("en" and "ru" are supported). You can change translation in templates by using twig tag trans and custom
   translation domain (see http://symfony.com/doc/current/book/translation.html).

To add custom calendar follow next steps:
1. Create CustomCalendar class, extends Garant\CalendarBundle\Model\Calendar
2. Define service for CustomCalendar class with tag garant_calendar.calendar

####Creating Custom Calendar
Custom calendar must implement Garant\CalendarBundle\Model\CalendarInterface. Methods getCode(), getName()
 and getUserIds() must be implemented. Method getUserIds() must return an array of id's of users, which
  have access to add events to calendar (for example [1,2,5,23]).
    
    <?php
    
    // CustomBundle/Calendar/CustomCalendar.php
    
    namespace CustomBundle\Calendar
    
    use Garant\CalendarBundle\Model\Calendar\Calendar;
    
    CustomCalendar extends Calendar
    {
        const CALENDAR_CODE = 'custom_calendar';
        
        public function getCode()
        {
            // this code identifies calendar
            return static::CALENDAR_CODE;
        }
        
        public function getName()
        {
            // some logic to create name, translation etc.
            
            // plain text in this example
            return 'Custom Calendar';
        }
        
        public function getUserIds()
        {
            $userIds = array();
            // some logic to create array of user id's
            
            return $userIds;
        }
        
        // other methods if you want
    }
    
####Service definition
    // app/config/services.yml
    
    garant_calendar.custom_calendar:
        class: CustomBundle\Calendar\CustomCalendar
        arguments: [Garant\CalendarBundle\Entity\CommonCalendarEvent]
        tags:
          - {name: garant_calendar.calendar}
Each calendar can use their event format. An event class specified in the argument
 of the calendar service. An event class must implement the interface
  Garant\CalendarBundle\Model\CalendarEventInterface.

### Console commands

Use

    garant:calendar:debug
to view a list of all active calendars. Output table contains rows with calendar code (returning by
 getCode() method), name (returning by getName() method) and calendar event class
  (from argument of calendar service). You can use <calendarCode> parameter to view
   information about calendar with <calendarCode>. For example:

    garant:calendar:debug diary

###Advanced custom calendars
The class of a custom calendar is not obliged to extend the abstract class Calendar, but it is obliged
 to implement interfaces Garant\CalendarBundle\Model\CalendarInterface and Garant\CalendarBundle\Model\EventChangableInterface.
 Method createCalendarEvent() must return an object of an event. The event class must implement the interface
  Garant\CalendarBundle\Model\CalendarEventInterface.

If a logic to create an array of user id's depends on the current user, a custom calendar should
implement Garant\CalendarBundle\Model\UserDependentInterface with setUser($userId) and getUser() methods.
To implement this methods you can use Garant\CalendarBundle\Traits\UserDependentTrait (works with instances
 of \FOS\UserBundle\Model\User and \FOS\UserBundle\Model\UserManager).

 By default, an array of users who have permission to view the calendar,
  coincides with the array of users with permission to add events.
  This array are returned by getUserIds(). To change the number of users
  who have permission to view the calendar, you must implement the interface
  Garant\CalendarBundle\Model\AccessCheckableInterface. Method checkAccess($userId) must
  return true or false if user has or is not have permission to view calendar.