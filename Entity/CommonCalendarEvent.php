<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 30.05.16
 * Time: 16:32
 */

namespace Garant\CalendarBundle\Entity;


class CommonCalendarEvent extends CalendarEvent implements \JsonSerializable
{
    public function jsonSerialize()
    {
        return [
            'id'        =>      $this->getId(),
            'title'     =>      $this->getName(),
            'comment'   =>      $this->getComment(),
            'start'     =>      $this->getDateTimeStart() ? $this->getDateTimeStart()->format(\DateTime::ISO8601) : '',
            'end'       =>      $this->getDateTimeStop() ? $this->getDateTimeStop()->format(\DateTime::ISO8601) : '',
            'allDay'    =>      $this->getAllDay(),
            'backgroundColor'     =>      $this->getBackgroundColor(),
            'borderColor'         =>      $this->getBorderColor() ?: $this->getBackgroundColor(),
            'textColor'           =>      $this->getFontColor(),
            'calendarCode'            =>      $this->getCalendarCode()
        ];
    }
}
