<?php
/**
 * This file is part of GarantECM project.
 * Created by: Mikhail Pegasin
 * Date: 08.06.16
 * Time: 18:42
 */

namespace Garant\CalendarBundle\Entity;

use Garant\CalendarBundle\Model\EventTypeInterface;

class EventType implements EventTypeInterface{
    protected $id;
    protected $name;
    protected $backgroundColor;
    protected $borderColor;
    protected $fontColor;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param $name string
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $bgdColor string
     * @return $this
     */
    public function setBackgroundColor($bgdColor)
    {
        $this->backgroundColor = $bgdColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * @param $brdColor string
     * @return $this
     */
    public function setBorderColor($brdColor)
    {
        $this->borderColor = $brdColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getBorderColor()
    {
        return $this->borderColor;
    }

    /**
     * @param $fntColor string
     * @return $this
     */
    public function setFontColor($fntColor)
    {
        $this->fontColor = $fntColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getFontColor()
    {
        return $this->fontColor;
    }
}