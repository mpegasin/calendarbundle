<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 29.05.16
 * Time: 17:05
 */

namespace Garant\CalendarBundle\Entity;

use Garant\CalendarBundle\Model\CalendarEvent as BaseCalendarEvent;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletable;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

class CalendarEvent extends BaseCalendarEvent 
{
    use SoftDeletable;
    use Timestampable;

    /**
     * @var int
     */
    protected $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CalendarEvent
     */
    public function setTitle($title)
    {
        $this->name = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return CalendarEvent
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateTimeStart
     *
     * @param \DateTime $dateTimeStart
     *
     * @return CalendarEvent
     */
    public function setDateTimeStart(\DateTime $dateTimeStart)
    {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    /**
     * Get dateTimeStart
     *
     * @return \DateTime
     */
    public function getDateTimeStart()
    {
        return $this->dateTimeStart;
    }

    /**
     * Set dateTimeStop
     *
     * @param \DateTime $dateTimeStop
     *
     * @return CalendarEvent
     */
    public function setDateTimeStop(\DateTime $dateTimeStop)
    {
        $this->dateTimeStop = $dateTimeStop;

        return $this;
    }

    /**
     * Get dateTimeStop
     *
     * @return \DateTime
     */
    public function getDateTimeStop()
    {
        return $this->dateTimeStop;
    }

    /**
     * Set creator
     *
     * @param \FOS\UserBundle\Model\User $creator
     *
     * @return CalendarEvent
     */
    public function setCreator(\FOS\UserBundle\Model\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \FOS\UserBundle\Model\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Get calendarCode
     *
     * @return string
     */
    public function getCalendarCode()
    {
        return $this->calendarCode;
    }

    /**
     * @param $allDay boolean
     * @return $this
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * @param $bgdColor string
     * @return $this
     */
    public function setBackgroundColor($bgdColor)
    {
        $this->backgroundColor = $bgdColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * @param $brdColor string
     * @return $this
     */
    public function setBorderColor($brdColor)
    {
        $this->borderColor = $brdColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getBorderColor()
    {
        return $this->borderColor;
    }

    /**
     * @param $fntColor string
     * @return $this
     */
    public function setFontColor($fntColor)
    {
        $this->fontColor = $fntColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getFontColor()
    {
        return $this->fontColor;
    }

    public function setEventType(EventType $eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    public function isDatesValid()
    {
        return $this->dateTimeStart <= $this->dateTimeStop;
    }

    /**
     * Set title (alias)
     *
     * @param string $title
     *
     * @return CalendarEvent
     */
    public function setName($title)
    {
        $this->name = $title;

        return $this;
    }

    /**
     * Get title (alias)
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateTimeStart
     *
     * @param \DateTime $dateTimeStart
     *
     * @return CalendarEvent
     */
    public function setStart(\DateTime $dateTimeStart)
    {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    /**
     * Get dateTimeStart
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->dateTimeStart;
    }

    /**
     * Set dateTimeStop
     *
     * @param \DateTime $dateTimeStop
     *
     * @return CalendarEvent
     */
    public function setEnd(\DateTime $dateTimeStop)
    {
        $this->dateTimeStop = $dateTimeStop;

        return $this;
    }

    /**
     * Get dateTimeStop
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->dateTimeStop;
    }

    /**
     * @param $fntColor string
     * @return $this
     */
    public function setTextColor($fntColor)
    {
        $this->fontColor = $fntColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getTextColor()
    {
        return $this->fontColor;
    }
}
