<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 31.05.16
 * Time: 17:49
 */

namespace Garant\CalendarBundle\Command;


use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as Command;

class CalendarDebugCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('garant:calendar:debug')
            ->setDescription('Return all calendar codes, names and event classes')
            ->addArgument(
                'code',
                InputArgument::OPTIONAL,
                'Name and event class of calendar with concrete code'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $code = $input->getArgument('code');
        $cm = $this->getContainer()->get('garant_calendar.calendar_manager');
        $text = '';
        $table = new Table($output);
        $table->setHeaders(['Code', 'Name', 'Event class']);
        if ($code) {
            try {
                $table->addRow([$code, $cm->getCalendarByCode($code)->getName(), $cm->getCalendarByCode($code)->getCalendarEventClass()]);
                $table->render();
            } catch(\Exception $e) {
                $text .= 'Календаря с кодом ' . $code . ' не существует.';
                $output->writeln($text);
            }

        } else {
            foreach ($cm->getCalendars() as $code => $calendar) {
                $table->addRow([$code, $calendar->getName(), $calendar->getCalendarEventClass()]);
                $table->render();
            }
        }




    }
}
