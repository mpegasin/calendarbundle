<?php

namespace Garant\CalendarBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class GarantCalendarExtension extends Extension implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('garant_calendar.creator_entity_class', $config['creator_entity_class']);
    }

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('doctrine.orm.listeners.resolve_target_entity')) {
            throw new \RuntimeException('doctrine.orm.listeners.resolve_target_entity must be defined');
        }
        $def = $container->findDefinition('doctrine.orm.listeners.resolve_target_entity');

        $def->addMethodCall(
            'addResolveTargetEntity',
            array('\FOS\UserBundle\Model\User',
                $container->getParameter('garant_calendar.creator_entity_class'), [])
        );

        if (!$container->has('garant_calendar.calendar_manager')) {
            return;
        }

        $definition = $container->findDefinition(
            'garant_calendar.calendar_manager'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'garant_calendar.calendar'
        );
        foreach ($taggedServices as $id => $tags) {
            $reflection = new \ReflectionClass($container->findDefinition($id)->getClass());
            if ($reflection->implementsInterface('\Garant\CalendarBundle\Model\CalendarInterface')) {
                $definition->addMethodCall(
                    'addCalendar',
                    array(new Reference($id))
                );
            }
        }
    }
}
