<?php
/**
 * This file is part of FullCalendarBundle project.
 * Created by: Mikhail Pegasin
 * Date: 30.05.16
 * Time: 16:05
 */

namespace Garant\CalendarBundle\Traits;

use FOS\UserBundle\Model\UserManager;
trait UserDependentTrait {

    /**
     * @var \FOS\UserBundle\Model\User
     */
    private $user;
    /**
     * @var UserManager
     */
    private $userManager;

    public function setUserManager(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function setUser($userId)
    {
        $this->user = $this->userManager->findUserBy(['id' => $userId]);

        return $this;
    }

    /**
     * @return \FOS\UserBundle\Model\User
     */
    public function getUser()
    {
        return $this->user;
    }
}